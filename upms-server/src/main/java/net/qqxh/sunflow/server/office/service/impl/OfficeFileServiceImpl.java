package net.qqxh.sunflow.server.office.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import net.qqxh.sunflow.server.office.bean.OfficeFile;
import net.qqxh.sunflow.server.office.mapper.OfficeFileMapper;
import net.qqxh.sunflow.server.office.service.OfficeFileService;
import org.springframework.stereotype.Service;

/**
 * Copyright (C), 2019-2020, sunflow开发团队
 * 在线编辑文件 服务类实现
 *
 * @fileName OfficeFileServiceImpl.java
 * @date     2019/5/25 15:57
 * @author   cjy
 */
@Service
public class OfficeFileServiceImpl extends ServiceImpl<OfficeFileMapper, OfficeFile> implements OfficeFileService {
}
